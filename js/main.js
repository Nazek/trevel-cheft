
$('.slider-top').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    infinite: true,
    fade: true,
    cssEase: 'linear'
});

var count = $('.text-img-bottom').length;
for (var k = 0; k < count; k++) {
    var text = $('.text-img-bottom').eq(k).text();
    arr = text.split('');
    var length = arr.length;
    if (length > 100) {
        $('.text-img-bottom').eq(k).text('');
        var string = "";
        for (var i = 0; i < 100; i++) {
            string += arr[i];
        }
        string += '...';
        $('.text-img-bottom').eq(k).text(string);
    }
}

$(document).ready(function() {
    $('.minus').click(function() {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function() {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

});

function Show_Div(Div_id) {

    $(Div_id).hide(0);

}
